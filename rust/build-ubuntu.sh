#!/bin/sh

mkdir -p ubuntu-target
docker build . -f ./UbuntuBuildDockerfile --tag ubuntu-harmony-build
docker run \
  --mount source="$(realpath ./ubuntu-target)",destination="/opt/harmony/target",type=bind \
  ubuntu-harmony-build "$@"
