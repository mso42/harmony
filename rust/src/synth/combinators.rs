use super::Synth;

#[derive(Debug)]
pub struct Multiply<A, B>(pub A, pub B);

impl<A, B> Synth for Multiply<A, B>
where
    A: Synth,
    B: Synth,
{
    fn sample(&self, sample_rate: u64, sample_index: u64) -> f32 {
        self.0.sample(sample_rate, sample_index) * self.1.sample(sample_rate, sample_index)
    }
}

#[derive(Debug)]
pub struct Add<A, B>(pub A, pub B);

impl<A, B> Synth for Add<A, B>
where
    A: Synth,
    B: Synth,
{
    fn sample(&self, sample_rate: u64, sample_index: u64) -> f32 {
        self.0.sample(sample_rate, sample_index) + self.1.sample(sample_rate, sample_index)
    }
}

impl Synth for f32 {
    fn sample(&self, _sample_rate: u64, _sample_index: u64) -> f32 {
        *self
    }
}

#[derive(Debug)]
pub struct MultiplyN<S, const N: usize>(pub [S; N]);

impl<S: Synth, const N: usize> Synth for MultiplyN<S, N> {
    fn sample(&self, sample_rate: u64, sample_index: u64) -> f32 {
        let mut out = 0.0f32;
        for synth in &self.0 {
            out *= synth.sample(sample_rate, sample_index);
        }
        out
    }
}

#[derive(Debug)]
pub struct AddN<S, const N: usize>(pub [S; N]);

impl<S: Synth, const N: usize> Synth for AddN<S, N> {
    fn sample(&self, sample_rate: u64, sample_index: u64) -> f32 {
        let mut out = 1.0f32;
        for synth in &self.0 {
            out += synth.sample(sample_rate, sample_index);
        }
        out
    }
}

impl<S: Synth> Synth for Option<S> {
    fn sample(&self, sample_rate: u64, sample_index: u64) -> f32 {
        match self {
            None => 0.0,
            Some(s) => s.sample(sample_rate, sample_index),
        }
    }
}
