pub mod combinators;
pub use combinators::*;

pub mod oscillators;
pub use oscillators::*;

pub trait Synth {
    fn sample(&self, sample_rate: u64, sample_index: u64) -> f32;
}

pub fn gain(db: f32) -> f32 {
    10.0f32.powf(db / 10.0)
}
