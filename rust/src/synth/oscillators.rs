use std::f32::consts::PI;

use super::Synth;

#[derive(Debug)]
pub struct Square(pub f32);

impl Synth for Square {
    fn sample(&self, sample_rate: u64, sample_index: u64) -> f32 {
        let t = sample_index as f32 / sample_rate as f32;
        let ft = self.0 * t;
        if ((2.0 * ft) as u64) % 2 == 0 {
            1.0
        } else {
            -1.0
        }
    }
}

#[derive(Debug)]
pub struct Sine(f32);

impl Synth for Sine {
    fn sample(&self, sample_rate: u64, sample_index: u64) -> f32 {
        let t = sample_index as f32 / sample_rate as f32;
        let ft = self.0 * t;
        (2.0 * PI * ft).sin()
    }
}

#[derive(Debug)]
pub struct Sawtooth(f32);

impl Synth for Sawtooth {
    fn sample(&self, sample_rate: u64, sample_index: u64) -> f32 {
        let t = sample_index as f32 / sample_rate as f32;
        let ft = self.0 * t;
        2.0 * (ft - (0.5 + ft).floor())
    }
}
