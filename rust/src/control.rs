pub type Instrument = u32;
pub type Pitch = Option<i32>;

pub struct ControlMessage {
    pub instrument: Instrument,
    pub pitch: Pitch,
}

pub trait ControlHandler<SynthT> {
    fn handle(&mut self, sample_index: u64, synth: &mut SynthT);
}

impl<SynthT> ControlHandler<SynthT> for () {
    fn handle(&mut self, _sample_index: u64, _synth: &mut SynthT) {}
}

use std::marker::PhantomData;
use std::sync::mpsc;

pub struct MpscMessageHandler<HandlerFn, SynthT> {
    receiver: mpsc::Receiver<ControlMessage>,
    handler: HandlerFn,
    _synth: PhantomData<SynthT>,
}

impl<HandlerFn, SynthT> MpscMessageHandler<HandlerFn, SynthT>
where
    HandlerFn: FnMut(ControlMessage, u64, &mut SynthT) -> (),
    SynthT: crate::Synth,
{
    pub fn new(handler: HandlerFn) -> (mpsc::Sender<ControlMessage>, Self) {
        let (sender, receiver) = mpsc::channel();
        (
            sender,
            MpscMessageHandler {
                handler,
                receiver,
                _synth: PhantomData,
            },
        )
    }
}

impl<SynthT, HandlerFn> ControlHandler<SynthT> for MpscMessageHandler<HandlerFn, SynthT>
where
    HandlerFn: FnMut(ControlMessage, u64, &mut SynthT) -> (),
    SynthT: crate::Synth,
{
    fn handle(&mut self, sample_index: u64, synth: &mut SynthT) {
        match self.receiver.try_recv() {
            Err(_) => (),
            Ok(message) => (self.handler)(message, sample_index, synth),
        }
    }
}
