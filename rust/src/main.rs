#![feature(array_map)]
#![feature(never_type)]

use std::io::{stdin, stdout};

use anyhow::Result;
use env_logger::Env;

pub mod output;
use output::OutputDevice;
pub mod synth;
use synth::*;
pub mod terminal;
use terminal::{TermDisplay, TermInput};
pub mod control;
use control::*;

fn main() -> Result<()> {
    env_logger::Builder::from_env(Env::default().default_filter_or("info")).try_init()?;

    let mut device = output::init_device(None)?;

    type Chanter = Option<Square>;
    let chanter: Chanter = None;

    type Drones = Multiply<f32, Add<Square, Square>>;
    let drones: Drones = Multiply(
        gain(-3.0),
        Add(
            Square(220.0),
            Square(330.0),
        ),
    );

    type Bagpipes = Add<Drones, Chanter>;
    let bagpipes = Add(drones, chanter);

    type QuietPipes = Multiply<f32, Bagpipes>;
    let quiet_pipes: QuietPipes = Multiply(gain(-10.0), bagpipes);

    let mut display = TermDisplay::new(stdout())?;
    display.write_message(format!("{:?}", quiet_pipes))?;

    let (control_tx, control_handler) =
        MpscMessageHandler::new(move |message, _sample_idex, synth: &mut QuietPipes| {
            let pitches = [
                880.0, 938.666, 1056.0, 1100.0, 1173.333, 1320.0, 1408.0, 1466.0, 1650.0,
            ]
            .map(|f| f / 2.0);
            let new_chanter = match message.pitch {
                None => None,
                Some(p) => {
                    let base_pitch_index = p.rem_euclid(pitches.len() as i32);
                    let base_pitch = pitches[base_pitch_index as usize];
                    let octave = p.div_euclid(pitches.len() as i32);
                    let freq: f32 = base_pitch * 2.0f32.powi(octave);
                    Some(Square(freq))
                }
            };
            synth.1 .1 = new_chanter;
            display.write_message(format!("{:?}", synth)).unwrap();
        });

    device.add_stream(quiet_pipes, control_handler)?;

    let input_handler = |message| -> Result<()> {
        control_tx.send(message)?;
        Ok(())
    };

    let term_input = TermInput::new(input_handler);
    term_input.run(stdin())?;
    // Prevent early deallocation
    drop(device);
    Ok(())
}
