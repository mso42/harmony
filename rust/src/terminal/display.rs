use std::io::Write;

use termion::raw::{IntoRawMode, RawTerminal};
use termion::{clear, cursor};

pub struct TermDisplay<W: Write> {
    term: cursor::HideCursor<RawTerminal<W>>,
}

impl<W: Write> TermDisplay<W> {
    pub fn new(out: W) -> std::io::Result<Self> {
        let hide_cursor_term = cursor::HideCursor::from(out.into_raw_mode()?);
        let mut this = Self {
            term: hide_cursor_term,
        };
        write!(this.term, "{}", clear::All)?;
        Ok(this)
    }

    pub fn write_message(&mut self, message: impl AsRef<str>) -> std::io::Result<()> {
        write!(
            self.term,
            "{}{}{}",
            cursor::Goto(1, 1),
            clear::All,
            message.as_ref()
        )?;
        self.term.flush()
    }
}

impl<W: Write> Drop for TermDisplay<W> {
    fn drop(&mut self) {
        write!(self.term, "{}{}", clear::All, cursor::Goto(1, 1)).unwrap();
    }
}
