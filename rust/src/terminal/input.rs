use std::collections::HashMap;
use std::io::Read;

use anyhow::Result;
use termion::event::{Event, Key};
use termion::input::TermRead;

use crate::control::{ControlMessage, Instrument, Pitch};

struct InputState {
    instrument: Instrument,
}

const INIT_STATE: InputState = InputState { instrument: 0 };

#[derive(Clone)]
enum KeyResponse {
    SelectInstrument(Instrument),
    Pitch(Pitch),
    Quit,
}

pub struct TermInput<H> {
    handler: H,
    keymap: HashMap<Key, KeyResponse>,
    input_state: InputState,
}

impl<H> TermInput<H>
where
    H: FnMut(ControlMessage) -> Result<()>,
{
    fn build_keymap() -> HashMap<Key, KeyResponse> {
        let mut keymap = HashMap::new();

        let instrument_keys = "qwer".chars();
        for (index, key) in instrument_keys.enumerate() {
            keymap.insert(Key::Char(key), KeyResponse::SelectInstrument(index as u32));
        }
        let pitch_keys = "7890-yuiophjkl;".chars();
        let pitches_start_at = -5;
        for (index, key) in pitch_keys.enumerate() {
            keymap.insert(
                Key::Char(key),
                KeyResponse::Pitch(Some(index as i32 + pitches_start_at)),
            );
        }
        let silence = Key::Char(' ');
        keymap.insert(silence, KeyResponse::Pitch(None));

        let quit = Key::Ctrl('c');
        keymap.insert(quit, KeyResponse::Quit);

        keymap
    }

    pub fn new(handler: H) -> Self {
        let keymap = Self::build_keymap();
        Self {
            handler,
            keymap,
            input_state: INIT_STATE,
        }
    }

    pub fn run(mut self, term: impl Read) -> Result<()> {
        for event in term.events() {
            if let Event::Key(key) = event? {
                let response_option: Option<KeyResponse> = self.keymap.get(&key).map(|s| s.clone());
                if let Some(response) = response_option {
                    use KeyResponse::*;
                    match response {
                        SelectInstrument(i) => {
                            self.input_state.instrument = i;
                        }
                        Pitch(p) => {
                            let message = ControlMessage {
                                instrument: self.input_state.instrument,
                                pitch: p,
                            };
                            (self.handler)(message)?;
                        }
                        Quit => break,
                    }
                }
            }
        }
        Ok(())
    }
}
