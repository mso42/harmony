use crate::{ControlHandler, Synth};

use anyhow::{anyhow, Result};
use cpal::traits::{DeviceTrait, HostTrait, StreamTrait};
use cpal::{SampleRate, StreamConfig};
use log::debug;

pub trait OutputDevice {
    type Stream;
    fn add_stream<SynthT: Synth + Send + 'static>(
        &mut self,
        synth: SynthT,
        control_handler: impl ControlHandler<SynthT> + Send + 'static,
    ) -> Result<()>;
    fn stream_count(&self) -> usize;
    fn sample_rate(&self) -> u32;
}

pub struct CpalOutputDevice<D: DeviceTrait> {
    config: StreamConfig,
    device: D,
    active_streams: Vec<D::Stream>,
}

pub fn init_device(desired_sample_rate: Option<u32>) -> Result<CpalOutputDevice<cpal::Device>> {
    let host = cpal::default_host();
    let device = host
        .default_output_device()
        .ok_or_else(|| anyhow!("no output device available"))?;
    let mut supported_configs = device.supported_output_configs()?;
    let supported_config_range = supported_configs
        .next()
        .ok_or_else(|| anyhow!("no supported config"))?;
    let config = match desired_sample_rate {
        None => supported_config_range.with_max_sample_rate(),
        Some(rate) => supported_config_range.with_sample_rate(SampleRate(rate)),
    }
    .config();
    Ok(CpalOutputDevice {
        config,
        device,
        active_streams: Vec::new(),
    })
}

impl<D> OutputDevice for CpalOutputDevice<D>
where
    D: DeviceTrait,
{
    type Stream = D::Stream;
    fn add_stream<SynthT: Synth + Send + 'static>(
        &mut self,
        mut synth: SynthT,
        mut control_handler: impl ControlHandler<SynthT> + Send + 'static,
    ) -> Result<()> {
        let mut sample_index: u64 = 0;
        let sample_rate = self.sample_rate() as u64;
        let stream = self.device.build_output_stream(
            &self.config,
            move |data: &mut [f32], _: &cpal::OutputCallbackInfo| {
                for sample in data.iter_mut() {
                    control_handler.handle(sample_index, &mut synth);
                    *sample = synth.sample(sample_rate, sample_index);
                    sample_index += 1;
                }
                debug!(
                    "Sample index = {}, buffer size = {}",
                    sample_index,
                    data.len()
                );
            },
            |_err| {},
        )?;
        stream.play()?;
        self.active_streams.push(stream);
        Ok(())
    }

    fn stream_count(&self) -> usize {
        self.active_streams.len()
    }

    fn sample_rate(&self) -> u32 {
        self.config.sample_rate.0
    }
}
