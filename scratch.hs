{-# LANGUAGE MultiParamTypeClasses #-}

import Data.Monoid (Product(..), Sum(..), Monoid, (<>))
import Data.Ratio ((%))

class Monoid m => Group m where
  invert :: m -> m
  pow :: Integral x => m -> x -> m
  pow m x
    | x < 0  = invert (pow m (-x))
    | x == 0 = mempty
    | x > 0  = m <> (pow m (x-1))

class Group g => Subgroup g h where
  liftG :: h -> g

leftCoset :: (Subgroup g h, Group g) => h -> g -> g
leftCoset g h = g <> (liftG h)

-- data Quotient g h = Quotient g h

-- class (Subgroup g h, Group h) => Quotient g h where

-------------------------------
-- Some groups and subgroups --
-------------------------------

instance Fractional a => Group (Product a) where
  invert p = (1/) <$> p

newtype PowerOfTwo a = PowerOfTwo { potExponent :: a }
  deriving (Show, Eq, Ord)

instance (Fractional a, Integral i) => Subgroup (Product a) (PowerOfTwo i) where
  liftG (PowerOfTwo x) = pow (Product 2) x

instance Integral i => Group (Sum i) where
  invert s = negate <$> s

data SmolSum = MinusOne | Zero | PlusOne

instance Integral i => Subgroup (Sum i) SmolSum where
  liftG MinusOne = Sum (-1)
  liftG Zero     = Sum 0
  liftG PlusOne  = Sum 1





-------------
-- Testing --
-------------

data TestCase a = TestCase { getLHS :: a, getRHS :: a}

assertEqual :: (Eq a, Show a) => TestCase a -> IO ()
assertEqual (TestCase a b)
  | a == b = return ()
  | otherwise = fail ((show a) ++ " did not equal " ++ (show b))

main = do
  assertEqual (test1 15)
  assertEqual (test2 3 15)
  putStrLn "nice job"

test1 :: Integral a => a -> TestCase (Product Rational)
test1 i = TestCase (invert (liftG (PowerOfTwo i))) (Product (1 % 2^i))

test2 :: (Integral a, Fractional b) => a -> b -> TestCase (Product b)
test2 a b = TestCase (leftCoset (PowerOfTwo a) (Product b)) (Product (2^a * b))
